#pragma once
#include "Gameboard.h"
#include "Deck.h"
#include "Card.h"

class Dealer {
private:
	Gameboard m_gameboard = Gameboard();
public:
	Dealer() = default;
	Dealer(const Gameboard& gameboard);
	~Dealer();

	void FillGameboard();

	bool F2(const unsigned& pos1, const unsigned& pos2, const unsigned& pos3);
	bool F1();
	void Routine();
};

