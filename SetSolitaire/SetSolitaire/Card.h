#pragma once
class Card {
public:
	enum class NUMBER { ONE, TWO, THREE, DEFAULT };
	enum class SYMBOL { DIAMOND, SQUIGGLE, OVAL, DEFAULT };
	enum class SHADING { SOLID, STRIPED, OPEN, DEFAULT };
	enum class COLOR { RED, GREEN, BLUE, DEFAULT };
private:
	NUMBER m_number = NUMBER::DEFAULT;
	SYMBOL m_symbol = SYMBOL::DEFAULT;
	SHADING m_shading = SHADING::DEFAULT;
	COLOR m_color = COLOR::DEFAULT;
public:
	Card() = default;
	Card(const NUMBER& number, const SYMBOL& symbol, const SHADING &shading, const COLOR &color);
	~Card();

	bool CheckNumber(const Card& card);
	bool CheckSymbol(const Card& card);
	bool CheckShading(const Card& card);
	bool CheckColor(const Card& card);
};

