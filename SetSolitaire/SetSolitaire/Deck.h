#pragma once
#include <iostream>
#include "Card.h"
#include <random>
#include <vector>
class Deck {
private:
	std::vector<Card> m_cards = std::vector<Card>();
	unsigned m_currentCardPosition = 0;
public:
	Deck() = default;
	~Deck();

	void FillDeck();
	void EmptyDeck();
	void ShuffleDeck();
	void swapCard(const unsigned& i, const unsigned& j);
	void ShuffleThreeTimes();
	Card GetNextCard();
	unsigned GetCurrentCardPosition() const;

};

