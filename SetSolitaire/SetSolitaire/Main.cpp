#include "Deck.h"
#include "Card.h"
#include "Gameboard.h"
#include "Dealer.h"
#include <iostream>


int main()
{
	Deck Francois;
	Francois.FillDeck();
	Francois.ShuffleThreeTimes();
	Gameboard Gerhart(Francois);
	Dealer Johnny(Gerhart);
	Johnny.Routine();
	//d.ShuffleDeck();
	return 0;
}