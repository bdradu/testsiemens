#include "Dealer.h"

Dealer::Dealer(const Gameboard & gameboard) :m_gameboard(gameboard)
{
}

Dealer::~Dealer()
{
}

void Dealer::FillGameboard()
{
	while (m_gameboard.CheckBoardMinimumSize())
		m_gameboard.DealNextCard();
}

bool Dealer::F2(const unsigned & pos1, const unsigned & pos2, const unsigned & pos3)
{
	Card c1 = m_gameboard.GetCard(pos1);
	Card c2 = m_gameboard.GetCard(pos2);
	Card c3 = m_gameboard.GetCard(pos3);

	if ((c1.CheckNumber(c2) && !c1.CheckNumber(c3)) || (c2.CheckNumber(c3) && !c2.CheckNumber(c1)) || (c1.CheckNumber(c3) && !c1.CheckNumber(c2)))
		return false;
	if ((c1.CheckSymbol(c2) && !c1.CheckSymbol(c3)) || (c2.CheckSymbol(c3) && !c2.CheckSymbol(c1)) || (c1.CheckSymbol(c3) && !c1.CheckSymbol(c2)))
		return false;
	if ((c1.CheckShading(c2) && !c1.CheckShading(c3)) || (c2.CheckShading(c3) && !c2.CheckShading(c1)) || (c1.CheckShading(c3) && !c1.CheckShading(c2)))
		return false;
	if ((c1.CheckColor(c2) && !c1.CheckColor(c3)) || (c2.CheckColor(c3) && !c2.CheckColor(c1)) || (c1.CheckColor(c3) && !c1.CheckColor(c2)))
		return false;
	return true;
}

bool Dealer::F1()
{
	unsigned boardSize = m_gameboard.GetBoardSize();
	for (unsigned i = 0; i < boardSize - 2; i++)
		for (unsigned j = i + 1; j < boardSize - 1; j++)
			for (unsigned k = j + 1; j < boardSize; k++)
			{
				if (F2(i, j, k))
				{
					m_gameboard.RemoveCards(i, j, k);
					return true;
				}
			}
	return false;
}

void Dealer::Routine()
{
	while (1)
	{
		F1();

		if (m_gameboard.GetCurrentCardPosition > 80)
		{
			std::cout << "S-a terminat jocul" << std::endl;
			return;
		}
		else
		{
			m_gameboard.DealNextCard();
			m_gameboard.DealNextCard();
			m_gameboard.DealNextCard();
		}


	}
}