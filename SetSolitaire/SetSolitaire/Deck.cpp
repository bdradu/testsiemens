#include "Deck.h"

Deck::~Deck()
{
	std::cout << "d";
}

void Deck::FillDeck()
{
	EmptyDeck();

	unsigned numberStart = static_cast<unsigned>(Card::NUMBER::ONE);
	unsigned numberFinal = static_cast<unsigned>(Card::NUMBER::DEFAULT);

	unsigned symbolStart = static_cast<unsigned>(Card::SYMBOL::DIAMOND);
	unsigned symbolFinal = static_cast<unsigned>(Card::SYMBOL::DEFAULT);

	unsigned shadingStart = static_cast<unsigned>(Card::SHADING::SOLID);
	unsigned shadingFinal = static_cast<unsigned>(Card::SHADING::DEFAULT);

	unsigned colorStart = static_cast<unsigned>(Card::COLOR::RED);
	unsigned colorFinal = static_cast<unsigned>(Card::COLOR::DEFAULT);

	for (unsigned number = numberStart; number < numberFinal; number++)
		for (unsigned symbol = symbolStart; symbol < symbolFinal; symbol++)
			for (unsigned shading = shadingStart; shading < shadingFinal; shading++)
				for (unsigned color = colorStart; color < colorFinal; color++)
				{
					m_cards.push_back(Card(static_cast<Card::NUMBER>(number),static_cast<Card::SYMBOL>( symbol),static_cast<Card::SHADING>( shading),static_cast<Card::COLOR>(color)));
				}


}

void Deck::EmptyDeck()
{
	m_cards.clear();
	m_currentCardPosition = 0;
}

void Deck::swapCard(const unsigned& i, const unsigned& j)
{
	Card aux = m_cards[i];
	m_cards[i] = m_cards[j];
	m_cards[j] = aux;
}

void Deck::ShuffleDeck()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, 51);
	for (int index = 0; index < m_cards.size(); index++)
	{
		int swapIndex = dis(gen);
		swapCard(swapIndex, index);
	}
}

void Deck::ShuffleThreeTimes()
{
	ShuffleDeck();
	ShuffleDeck();
	ShuffleDeck();
}

Card Deck::GetNextCard()
{
	return m_cards[m_currentCardPosition++];
}

unsigned Deck::GetCurrentCardPosition() const
{
	return m_currentCardPosition;
}
