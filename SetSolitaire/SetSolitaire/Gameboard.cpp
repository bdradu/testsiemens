#include "Gameboard.h"

Gameboard::Gameboard(const Deck & deck) : m_deck(deck)
{
}

Gameboard::~Gameboard()
{
}

bool Gameboard::CheckBoardMinimumSize() const
{
	return m_currentCards.size() < 12;
}

Card Gameboard::GetCard(const unsigned& position) const
{
	return m_currentCards[position];
}

unsigned Gameboard::GetBoardSize() const
{
	return m_currentCards.size();
}

void Gameboard::RemoveCards(const unsigned & pos1, const unsigned & pos2, const unsigned & pos3)
{
	m_currentCards.erase(m_currentCards.begin() + pos1);
	m_currentCards.erase(m_currentCards.begin() + pos2 - 1);
	m_currentCards.erase(m_currentCards.begin() + pos3 - 2);
}


void Gameboard::DealNextCard()
{
	m_currentCards.push_back(m_deck.GetNextCard());
}

unsigned Gameboard::GetCurrentCardPosition() const
{
	return m_deck.GetCurrentCardPosition();
}


