#include "Card.h"

 
Card::Card(const NUMBER & number, const SYMBOL & symbol, const SHADING & shading, const COLOR & color):m_number(number), m_symbol(symbol), m_shading(shading), m_color(color)
{

}

Card::~Card()
{
}

bool Card::CheckNumber(const Card & card)
{
	return m_number==card.m_number;
}

bool Card::CheckSymbol(const Card & card)
{
	return m_symbol==card.m_symbol;
}

bool Card::CheckShading(const Card & card)
{
	return m_shading==card.m_shading;
}

bool Card::CheckColor(const Card & card)
{
	return m_color == card.m_color;
}
