#pragma once
#include "Deck.h"
#include "Card.h"
#include <vector>
class Gameboard {
private:
	Deck m_deck=Deck();
	std::vector<Card> m_currentCards = std::vector<Card>();
public:
	Gameboard() = default;
	Gameboard(const Deck& deck);
	~Gameboard();
	
	bool CheckBoardMinimumSize()const;
	Card GetCard(const unsigned& position)const;
	unsigned GetBoardSize() const;
	void RemoveCards(const unsigned& pos1, const unsigned& pos2, const unsigned& pos3);
	void DealNextCard();
	unsigned GetCurrentCardPosition()const;
};

